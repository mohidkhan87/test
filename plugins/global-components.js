import Vue from 'vue'

import PrimaryButton from '@/components/ui/PrimaryButton'

Vue.component('PrimaryButton', PrimaryButton)