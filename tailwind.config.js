module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#3182CE',
        primaryHover: '#2B6CB0',
        secondary: '#319795',
        secondaryHover: '#2C7A7B',
        primaryActive: '#2C5282',
        secondaryActive: '#285E61',
        blackish: '#2D3748',
        lightBlue: '#EBF4FF',
        skyBlue: '#E6FFFA',
        skyBlueActive: '#81E6D9',
        lightestGray: '#F7FAFC',
        lightGray: '#00000033',
        darkGray: '#718096',
        borderGray: '#CBD5E0'
      },
      fontSize: {
        "42px": '42px'
      },
      height: {
        '5px': '5px',
        '67px': '67px'
      },
      margin: {
        '17px':'17px'
      },
      borderWidth: {
        '0.5px': '0.5px'
      },
      boxShadow: {
        bottomRounded: '0px 3px 6px #00000029;',
        topRounded: '0px -1px 3px #00000033;'
      },
      letterSpacing: {
        '1.26px': '1.26px;',
        '0.84px': '0.84px'
      },
      borderRadius: {
        '50%': '50%'
      },
      backgroundImage: {
        heroPoster: "url('assets/img/home/poster.svg')",
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
